NAME := get.codedin.wales
VERSION := latest

.PHONY: build
build:
	docker build -t quay.io/picymru/${NAME}:${VERSION} .

.PHONY: push
push:
	docker push quay.io/picymru/${NAME}:${VERSION}

.PHONY: remove
remove:
	docker rmi quay.io/picymru/${NAME}:${VERSION}