# _picymru
header() {
echo -e "
    ____  _ ______                          
   / __ \(_) ____/_  ______ ___  _______  __
  / /_/ / / /   / / / / __  __ \/ ___/ / / /
 / ____/ / /___/ /_/ / / / / / / /  / /_/ / 
/_/   /_/\____/\__  /_/ /_/ /_/_/   \____/  
              /____/                        
======================================================

This script will download and install $productname to 
your Raspberry Pi, plus any supporting documentation 
or examples.

Always be careful when running scripts and commands 
copied in from the internet. Ensure they are from a
trusted source.

If you want to see what this script does before running
it, you should run:

  \`curl $baseurl/$productname/install.sh\`
"
}
