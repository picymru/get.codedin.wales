# _vars
FORCE=$1
ASK_TO_REBOOT=false
CURRENT_SETTING=false
MIN_INSTALL=false
FAILED_PKG=false
REMOVE_PKG=false
UPDATE_DB=false

AUTOSTART=~/.config/lxsession/LXDE-pi/autostart
BOOTCMD=/boot/cmdline.txt
CONFIG=/boot/config.txt
DTBODIR=/boot/overlays
APTSRC=/etc/apt/sources.list
INITABCONF=/etc/inittab
BLACKLIST=/etc/modprobe.d/raspi-blacklist.conf
LOADMOD=/etc/modules

RASPOOL="http://mirrordirector.raspbian.org/raspbian/pool"
RPIPOOL="http://archive.raspberrypi.org/debian/pool"
DEBPOOL="http://ftp.debian.org/debian/pool"
GETPOOL="https://get.codedin.wales/"
