#!/bin/bash

% include('_common/_general.tpl')

% include('_common/_os.tpl')

% include('_common/_picymru.tpl')

% include('_common/_vars.tpl')

productname="webbot" # the name of the product to install
scriptname="webbot" # the name of this script
spacereq=2 # minimum size required on root partition in MB
debugmode="no" # whether the script should use debug routines
debuguser="none" # optional test git user to use in debug mode
debugpoint="none" # optional git repo branch or tag to checkout
forcesudo="no" # whether the script requires to be ran with root privileges
promptreboot="no" # whether the script should always prompt user to reboot
mininstall="no" # whether the script enforces minimum install routine
customcmd="no" # whether to execute commands specified before exit
gpioreq="yes" # whether low-level gpio access is required
i2creq="no" # whether the i2c interface is required
i2sreq="no" # whether the i2s interface is required
spireq="no" # whether the spi interface is required
uartreq="no" # whether uart communication is required
armhfonly="yes" # whether the script is allowed to run on other arch
armv6="yes" # whether armv6 processors are supported
armv7="yes" # whether armv7 processors are supported
armv8="yes" # whether armv8 processors are supported
raspbianonly="no" # whether the script is allowed to run on other OSes
osreleases=( "Raspbian" "Kano" "Mate" "PiTop" "RetroPie" ) # list os-releases supported
oswarning=( "OSMC" "Volumio" ) # list experimental os-releases
osdeny=( "Darwin" "Debian" "Ubuntu" "Kali" "Linaro" ) # list os-releases specifically disallowed
debpackage="webbot" # the name of the package in apt repo
piplibname="webbot" # the name of the lib in pip repo
pipoverride="no" # whether the script should give priority to pip repo
pip2support="yes" # whether python2 is supported
pip3support="yes" # whether python3 is supported
topdir="picymru" # the name of the top level directory
localdir="webbot" # the name of the dir for copy of resources
gitreponame="webbot" # the name of the git project repo
gitusername="picymru" # the name of the git user to fetch repo from
gitrepobranch="master" # repo branch to checkout
gitrepotop="root" # the name of the dir to base repo from
gitrepoclone="no" # whether the git repo is to be cloned locally
gitclonedir="source" # the name of the local dir for repo
repoclean="no" # whether any git repo clone found should be cleaned up
repoinstall="no" # whether the library should be installed from repo
libdir="library" # subdirectory of library in repo
copydir=( "documentation" "examples" ) # subdirectories to copy from repo
pkgremove=() # list of conflicting packages to remove
coredeplist=() # list of core dependencies
copyhead="no" # whether to use the latest repo commit or release tag
pythondep=() # list of python dependencies
pipdeplist=() # list of dependencies to source from pypi
examplesdep=() # list of python modules required by examples
somemoredep=() # list of additional dependencies
xdisplaydep=() # list of dependencies requiring X server

header
if confirm "Are you sure you wish to continue?"; then
	run_checks
else
	inform "Aborting..."
fi